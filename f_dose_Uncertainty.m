function [] = f_dose_Uncertainty(input_parameter)
%f_dose_Uncertainty(input_parameter) Calculation of dose
%uncertainty maps
%   Calculations with Dose Gradients, Uncertainty Maps of DIRs and Angles
%   Calculates and saves: - Gradient of Dose Images (Sobel, Prewitt) (Gy/mm)
%                       - Gradient times Uncertainty Map DIRs (Max-Min of
%                         radii of the DVFs vectors (mm)) (Gy)
%                       - Gradient times Uncertainty Map times Direction
%                       Factor
%                       - Difference between Dose difference (DD) of the six DIR
%                         methods to the gradient (G) respectively to the gradient
%                         times the uncertainty (GU), resp. to the gradient
%                         times the modelled uncertainty (GMU)
%                       - Histogram of the difference between DD and G,
%                       resp GU, GMU
% Used functions:       readmha , imgradient3 (matlab),
%                       imgradientxyz (matlab)
%   Code from: Florian Amstutz et al 2021 Phys. Med. Biol. 66 105007
%   Please cite if you use this code, for research purposes only
%   florian.amstutz@psi.ch

for k=1:length(input_parameter.Patients)
    patient_number = string(input_parameter.Patients{k});
    patient_string = strcat('Patient_',patient_number);
    disp(patient_string)
    for i=1:length(input_parameter.CTnumbers)
        CT_string = strcat('CT',string(input_parameter.CTnumbers{i}));
        disp(CT_string)
        pathStructures = strcat(input_parameter.structures_base_path,patient_string,'/CTs/MHAs/0/structures/');
        
     	%If all structures should be selected iterate over files and get also structure name out
        structure_name_cell = input_parameter.Structures;
        if isempty(structure_name_cell)
            files = dir(strcat(pathStructures,'*.mha'));
            for file = files'
                structure_name = erase(file.name,'.mha');
                structure_name_cell{end+1} = structure_name;
            end
        end

        for j=1:length(structure_name_cell)
            structure_name = string(structure_name_cell(j));
            CT_num = string(input_parameter.CTnumbers(i));

            for l=1:length(input_parameter.method_letters)
                method_letters = string(input_parameter.method_letters{l});
                
                % Check if already all data calculated
                all_data_produced = 0;
                if strcmp(input_parameter.recalculation_flag,'Off')
                    all_data_produced = check_data(input_parameter,patient_string,CT_string,CT_num,structure_name,method_letters);                    
                end
                if all_data_produced == 1
                    continue
                end
                
                %% Load data (i.e. dose, uncertainty maps, dose diff. from six DIR methods)
                dosefile = strcat(input_parameter.data_base_path,'Data/',patient_string,'/Dose/',...
                    'Dose',CT_num,'.mha'); % Path Dosefile
                [Dose,Info] = readmha(dosefile); % Read dose file
                
                if contains(input_parameter.uncertainty_method,'Created')
                 	uncertaintyfile = strcat(input_parameter.results_base_path,patient_string,...
                        '/DIR_Uncertainty_Map/',input_parameter.uncertainty_method,'/Methods_',input_parameter.method_letters,...
                        '/Uncertainty_Measure_',input_parameter.uncertainty_method,'_r_CT',CT_num,'.mha'); % Path Uncertainty map max-min r
                    [Uncertainty,~] = readmha(uncertaintyfile); % Load uncertainty map std. dev. r
                else
                    uncertaintyfile = strcat(input_parameter.results_base_path,patient_string,...
                        '/DIR_Uncertainty_Map/',input_parameter.uncertainty_method,'/Methods_',input_parameter.method_letters,...
                        '/Uncertainty_Measure_',input_parameter.uncertainty_method,'_CT',CT_num,'.mha'); % Path Uncertainty map max-min r
                    [Uncertainty,~] = readmha(uncertaintyfile); % Load uncertainty map std. dev. r
                end
                
              	uncertainty_direction_xyz = strcat(input_parameter.data_base_path,'Data/',patient_string,...
                     '/DIRs/',input_parameter.uncertainty_direction_factor,'/',input_parameter.uncertainty_direction_factor,'_0_',CT_num,'.mha'); % Path Uncertainty 
             	[Uncertainty_direction, ~] = readmha(uncertainty_direction_xyz);
              
                
                %% Mask for Histogram

                datafileStructure = strcat(input_parameter.structures_base_path,patient_string,'/CTs/MHAs/0/structures/',string(structure_name),'.mha');
                if isfile(datafileStructure)
                    datafileStructure = datafileStructure;
                else
                    error(strcat("No structure in the following location",datafileStructure));
                end
                [StructureMask,~] = readmha(datafileStructure);
                idx1 = find(StructureMask); % Only inside body structure

                datafileStructureBody = strcat(input_parameter.structures_base_path,patient_string,'/CTs/MHAs/0/structures/BODY.mha');
                [StructureMaskBody,~] = readmha(datafileStructureBody);
                idxBody = find(StructureMaskBody); % Only inside body structure

                idxDose = find(Dose ~= 0); % Only dose region

                idx = intersect(idx1,idxDose);
                idxBody = intersect(idxBody,idxDose);

                
                %% Gradient calculations                            
                [Gmag_sobel,Gaz_sobel,Gele_sobel] = imgradient3(Dose); % Gradients sobel filter
                Gaz_sobel = deg2rad(Gaz_sobel);
                Gele_sobel = deg2rad(Gele_sobel);
                Gradient_direction = cat(4,Gmag_sobel,Gaz_sobel,Gele_sobel);
                Gradient_direction_xyz = f_vectorfield2cartesiancoord(Gradient_direction);               

                Gmag_sobel_copy = Gmag_sobel;
                Gmag_sobel_copy(setdiff(1:end,idxBody)) = 0;
                Gmag_sobel_max = max(Gmag_sobel_copy,[],'all');
                
                Gmag_sobel(setdiff(1:end,idx)) = NaN;
                Gaz_sobel(setdiff(1:end,idx)) = NaN;
                Gele_sobel(setdiff(1:end,idx)) = NaN;
                
                %% Direction factor Dose Gradient to "Uncertainty Direction"
                Ud_x = Uncertainty_direction(:,:,:,1);
                Ud_y = Uncertainty_direction(:,:,:,2);
                Ud_z = Uncertainty_direction(:,:,:,3);
                Gd_x = Gradient_direction_xyz(:,:,:,1);
                Gd_y = Gradient_direction_xyz(:,:,:,2);
                Gd_z = Gradient_direction_xyz(:,:,:,3);
                
                
                Direction_factor = (Ud_x.*Gd_x+Ud_y.*Gd_y+Ud_z.*Gd_z)./(sqrt(Ud_x.^2+Ud_y.^2+Ud_z.^2).*sqrt(Gd_x.^2+Gd_y.^2+Gd_z.^2));
                Direction_factor = abs(Direction_factor);
                Direction_factor = smooth3(Direction_factor);
                
                Direction_factor(setdiff(1:end,idxBody)) = NaN;
                
              	% Save Direction Factor map
                writefile = strcat(input_parameter.results_base_path,'/',patient_string,'/Dose_Uncertainty_Method/Direction_Factor/Methods_',...
                    method_letters,'/');
                f_create_folder(writefile);
                writefile = strcat(writefile,'Direction_Factor_',input_parameter.uncertainty_method,'_',...
                    input_parameter.uncertainty_direction_factor,'_',structure_name,'_CT',CT_num,'.mha');
                if ~isfile(writefile) || strcmp(input_parameter.recalculation_flag,'On')
                    writemha(writefile,Direction_factor,Info.Offset,Info.ElementSpacing,'float');
                end
                
             
                %% Save dose gradient images

                % Save Sobel dose gradient image
                writefile = strcat(input_parameter.results_base_path,'/',patient_string,...
                    '/Dose_Uncertainty_Method/Dose_Gradients/');
                f_create_folder(writefile);
                writefile = strcat(writefile,'Gradients_mag_sobel_',structure_name,'_CT',CT_num,'.mha');
                if ~isfile(writefile) || strcmp(input_parameter.recalculation_flag,'On')
                    writemha(writefile,Gmag_sobel./44,Info.Offset,Info.ElementSpacing,'float');
              	end
                
              	% Save Sobel dose gradient image
                writefile = strcat(input_parameter.results_base_path,'/',patient_string,...
                    '/Dose_Uncertainty_Method/Dose_Gradients/');
                f_create_folder(writefile);
                writefile = strcat(writefile,'Gradients_phi_sobel_',structure_name,'_CT',CT_num,'.mha');
                %writemha(writefile,Gaz_sobel./44,Info.Offset,Info.ElementSpacing,'float');
                if ~isfile(writefile) || strcmp(input_parameter.recalculation_flag,'On')                
                    writemha(writefile,Gaz_sobel,Info.Offset,Info.ElementSpacing,'float');
                end
                
              	% Save Sobel dose gradient image
                writefile = strcat(input_parameter.results_base_path,'/',patient_string,...
                    '/Dose_Uncertainty_Method/Dose_Gradients/');
                f_create_folder(writefile);
                writefile = strcat(writefile,'Gradients_theta_sobel_',structure_name,'_CT',CT_num,'.mha');
                %writemha(writefile,Gele_sobel./44,Info.Offset,Info.ElementSpacing,'float');
             	if ~isfile(writefile) || strcmp(input_parameter.recalculation_flag,'On')
                    writemha(writefile,Gele_sobel,Info.Offset,Info.ElementSpacing,'float');
                end
                
                % Save Sobel dose gradient image
                writefile = strcat(input_parameter.results_base_path,'/',patient_string,...
                    '/Dose_Uncertainty_Method/Dose_Gradients/');
                f_create_folder(writefile);
                writefile = strcat(writefile,'Gradients_mag_sobel_x',structure_name,'_CT',CT_num,'.mha');
            	if ~isfile(writefile) || strcmp(input_parameter.recalculation_flag,'On')    
                    writemha(writefile,Gd_x./44,Info.Offset,Info.ElementSpacing,'float');
                end
                
               	% Save Sobel dose gradient image
                writefile = strcat(input_parameter.results_base_path,'/',patient_string,...
                    '/Dose_Uncertainty_Method/Dose_Gradients/');
                f_create_folder(writefile);
                writefile = strcat(writefile,'Gradients_mag_sobel_y',structure_name,'_CT',CT_num,'.mha');
                if ~isfile(writefile) || strcmp(input_parameter.recalculation_flag,'On')                
                    writemha(writefile,Gd_y./44,Info.Offset,Info.ElementSpacing,'float');
                end
                
             	% Save Sobel dose gradient image
                writefile = strcat(input_parameter.results_base_path,'/',patient_string,...
                    '/Dose_Uncertainty_Method/Dose_Gradients/');
                f_create_folder(writefile);
                writefile = strcat(writefile,'Gradients_mag_sobel_z',structure_name,'_CT',CT_num,'.mha');
                if ~isfile(writefile) || strcmp(input_parameter.recalculation_flag,'On')                
                    writemha(writefile,Gd_z./44,Info.Offset,Info.ElementSpacing,'float');
                end


                %% Calculate dose gradient times uncertainty map images

                Grad_Uncer_mag_Sobel = (Gmag_sobel./44).*Uncertainty; % Magnitude gradients sobel times uncertainty 

                Grad_Uncer_Angle_mag_Sobel = (Gmag_sobel./44).*Uncertainty.*(Direction_factor); % Magnitude gradients sobel times uncertainty and direction factor
                clear Gmag_sobel Uncertainty Direction_factor Gd_z Gd_y Gd_x Gele_sobel Gaz_sobel
                %% Save dose gradients times uncertainty map images

                % Save gradient magnitude Sobel multiplied with uncertainty map
                writefolder = strcat(input_parameter.results_base_path,'/',patient_string,...
                    '/Dose_Uncertainty_Method/Dose_Uncertainty/Methods_',method_letters,'/');
                f_create_folder(writefolder);
                               
                writefile = strcat(writefolder,'GMU_',input_parameter.uncertainty_direction_factor,'_',input_parameter.uncertainty_method,'_',structure_name,'_CT_',CT_num,'.mha');
                if ~isfile(writefile) || strcmp(input_parameter.recalculation_flag,'On') 
                    writemha(writefile,Grad_Uncer_Angle_mag_Sobel,Info.Offset,Info.ElementSpacing,'float');
                end                
   

                clear Dose Dosediff Dosediffrescaled Gaz_sobel Gele_sobel Gmag_sobel_upscaled Grad_Uncer_mag_Sobel

            end %end method_letters_cell
            
      
        end % end CT numbers loop
    end % end patients loop

clear all

end % end of function

