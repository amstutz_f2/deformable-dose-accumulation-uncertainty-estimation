function [] = f_MaxMin_DVF(data_base_path,results_base_path,data_struct,...
    patient_string,methods,method_letters,CT_number,CT_string)
%f_MaxMin_DVF Calculates Max,Min and Max-Min matrix in spherical coord.
%   Code from: Florian Amstutz et al 2021 Phys. Med. Biol. 66 105007
%   Please cite if you use this code, for research purposes only
%   florian.amstutz@psi.ch
    
    % Iterate over included methods
    for j=1:length(methods)
        method_string = string(methods{j});

        vectorfield_datafile = strcat(data_base_path,patient_string,'/DIRs/',...
           method_string,'/',method_string,'_0','_',CT_number,'.mha'); 
        [data_struct.(patient_string).(method_string).(CT_string),Info] = readmha(vectorfield_datafile); % Read vectorfield data
    end % end loop over methods


    datafileBody = strcat(data_base_path,patient_string,'/CTs/MHAs/0/structures/BODY.mha');
    [BodyMask,~] = readmha(datafileBody);
	ind = find(BodyMask);
    Body_index_indices= ind;
    
    % Iterate over included DIR methods
    for l=1:length(methods)
        method_name = string(methods(l));
        % Get the values out of the struct and transform to spherical
        % coordinates
        SpherCoordMatrix = f_vectorfield2sphericalcoord(data_struct.(patient_string).(method_name).(CT_string));
        SpherCoordMatrix_r = SpherCoordMatrix(:,:,:,3);
        
        % Find the Max and Min Matrix
        if l==1
            MaxMatrix = SpherCoordMatrix_r;
            MinMatrix = SpherCoordMatrix_r;
        else
            MaxMatrix = max(SpherCoordMatrix_r, MaxMatrix);
            MinMatrix = min(SpherCoordMatrix_r, MinMatrix);
        end            
    end
    
    % Calculate Max-Min
    MaxMinusMinMatrix_r = MaxMatrix-MinMatrix;
    MaxMinusMinMatrix_r(setdiff(1:end,Body_index_indices)) = NaN;

    writefile_base = strcat(results_base_path,'/',patient_string,'/DIR_Uncertainty_Map/Max-Min/Methods_',method_letters,'/');

    f_create_folder(writefile_base);
    writefile = strcat(writefile_base,'Max_CT',CT_number,'.mha');
    writemha(writefile,MaxMatrix,Info.Offset,Info.ElementSpacing,'float');

    writefile = strcat(writefile_base,'Min_CT',CT_number,'.mha');
    writemha(writefile,MinMatrix,Info.Offset,Info.ElementSpacing,'float');

    writefile = strcat(writefile_base,'Uncertainty_Measure_Max-Min_CT',CT_number,'.mha');
    writemha(writefile,MaxMinusMinMatrix_r,Info.Offset,Info.ElementSpacing,'float');

    MaxMinusMinMatrix_r= MaxMinusMinMatrix_r(Body_index_indices);    
    MaxMinusMin_flat = reshape(MaxMinusMinMatrix_r,[],1);

    writematrix_base = strcat(results_base_path,patient_string,'/Correlation_Data/Methods_',method_letters,'/');
    f_create_folder(writematrix_base);
    writematrix(MaxMinusMin_flat,strcat(writematrix_base,'Max-Min_CT',CT_number,'.txt'));
       
end % end function

