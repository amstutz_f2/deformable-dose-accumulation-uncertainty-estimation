function [outputvectorfield] = f_vectorfield2sphericalcoord(inputvectorfield)
%f_vectorfield2sphericalcoord Converts a vectorfield in cartesian
%coordinates to one spherical coordinates
%   Takes a vectorfield in cartesian coordinates and transforms the
%   vectorfield to spherical coordinates

x_component = inputvectorfield(:,:,:,1);
y_component = inputvectorfield(:,:,:,2);
z_component = inputvectorfield(:,:,:,3);

[phi,theta,r] = cart2sph(x_component,y_component,z_component);

clear x_component y_component z_component

outputvectorfield = cat(4,phi,theta,r);
end

