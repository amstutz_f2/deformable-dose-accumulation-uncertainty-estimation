function [] = f_modelled_geo_Uncertainty(input_parameter)
%f_modelled_geo_Uncertainty Calculates the uncertainty maps from the
%linear model
%   Code from: Florian Amstutz et al 2021 Phys. Med. Biol. 66 105007
%   Please cite if you use this code, for research purposes only
%   florian.amstutz@psi.ch

%% Patient Specific Correlation

    for i=1:length(input_parameter.Patients)
        cur_patient = string(input_parameter.Patients(i));
        cur_patient_name = strcat('Patient_',cur_patient);
        data.(cur_patient_name) = [];

        predictors = [];
        uncertainties = [];

        for j=1:length(input_parameter.prediction_CTnumbers)
            cur_CT = string(input_parameter.prediction_CTnumbers(j));
            cur_CT_name = strcat('CT',cur_CT);
            data.(cur_patient_name).(cur_CT_name) = [];
            data.(cur_patient_name).(cur_CT_name).predictor = [];

            filename_predictor_DVFs = strcat(input_parameter.results_base_path, 'Patient_', cur_patient,...
                   '/Correlation_Data/','Methods_',input_parameter.method_letters,'/',input_parameter.predictor_type,'_r_CT',cur_CT,'.txt');                       
            imported_predictor = importdata(filename_predictor_DVFs);
            imported_predictor = abs(imported_predictor);
            predictors = vertcat(predictors,imported_predictor);


            filename_Uncertainty_Measures = strcat(input_parameter.results_base_path, 'Patient_', cur_patient,...
             '/Correlation_Data/Methods_',input_parameter.method_letters,'/',input_parameter.predictor_uncertainty,'_CT',cur_CT,'.txt');
            imported_Uncertainty_Measures = (importdata(filename_Uncertainty_Measures));
            uncertainties = vertcat(uncertainties,imported_Uncertainty_Measures);

            clear imported_Uncertainty_Measures imported_predictor

        end

        [b,bint] = regress(uncertainties(1:end),predictors(1:end));

        fileIDFolderName = strcat(input_parameter.results_base_path,'Correlation_Factors/');
        f_create_folder(fileIDFolderName);
        fileIDFolderName = strcat(fileIDFolderName,'Methods_',input_parameter.method_letters{1});
        f_create_folder(fileIDFolderName);
        fileID = fopen(strcat(fileIDFolderName,'/Correlation_Factor_',cur_patient_name,'_',...
        strrep(join(input_parameter.prediction_CTnumbers),' ','_'),'_',input_parameter.predictor_type,'_',input_parameter.predictor_uncertainty,'.txt'),'w');
        fprintf(fileID,string(b));
        fclose(fileID);

    end    

    clearvars -except input_parameter   


%% Transfer from one specific DVF to Uncertainty Map

for i=1:length(input_parameter.Patients)
    cur_patient = string(input_parameter.Patients(i));
    cur_patient_name = strcat('Patient_',cur_patient);
    
    file_name = strcat(input_parameter.results_base_path,'Correlation_Factors/',...
            'Methods_',input_parameter.method_letters{1},'/Correlation_Factor_',...
            cur_patient_name,'_',strrep(join(input_parameter.prediction_CTnumbers),' ','_'),...
            '_',input_parameter.predictor_type,'_',input_parameter.predictor_uncertainty,'.txt');
    fileID = fopen(file_name,'r');
    formatSpec = '%f';
    factor_r = fscanf(fileID,formatSpec);
    empiric_factor = 1;
    factor_r = factor_r*empiric_factor;
    
   	for j=1:length(input_parameter.create_CTnumbers)
        create_CT = string(input_parameter.create_CTnumbers(j));
        
        datafileDVF = strcat(input_parameter.data_base_path,'Data/',cur_patient_name,'/DIRs/',...
            input_parameter.predictor_type,'/',input_parameter.predictor_type,'_0_',create_CT,'.mha');
        [vectorfield,StructureMaskInfo] = readmha(datafileDVF);
        vectorfield_mag = f_vectorfield2sphericalcoord(vectorfield);

        datafileBody = strcat(input_parameter.data_base_path,'Data/',cur_patient_name,'/CTs/MHAs/0/structures/BODY.mha');
        [BodyMask,~] = readmha(datafileBody);
        ind = find(BodyMask);

        created_mag = vectorfield_mag(:,:,:,3);
        created_mag(setdiff(1:end,ind))=0;
        created_mag = created_mag*factor_r;

        direction_theta =vectorfield_mag(:,:,:,2);
        direction_phi =vectorfield_mag(:,:,:,1);
        direction_theta(setdiff(1:end,ind))=0;
        direction_phi(setdiff(1:end,ind))=0;

        writefolder = strcat(input_parameter.results_base_path,cur_patient_name,...
            '/DIR_Uncertainty_Map/Created_Uncertainty_Map_',input_parameter.predictor_type,'/Methods_',input_parameter.method_letters,'/');
        f_create_folder(writefolder);
        writefile = strcat(writefolder,'Uncertainty_Measure_Created_Uncertainty_Map_',...
            input_parameter.predictor_type,'_r_CT',create_CT,'.mha');
        writemha(writefile,created_mag,StructureMaskInfo.Offset,StructureMaskInfo.ElementSpacing,'float');

        total_matrix = cat(4,direction_phi,direction_theta,created_mag);
        total_matrix_xyz = f_vectorfield2cartesiancoord(total_matrix);
        writefile = strcat(writefolder,'Uncertainty_Measure_Created_Uncertainty_DVF_',...
            input_parameter.predictor_type,'_CT',create_CT,'.mha');
        writemha(writefile,total_matrix_xyz,StructureMaskInfo.Offset,StructureMaskInfo.ElementSpacing,'float');
    end
end

end


