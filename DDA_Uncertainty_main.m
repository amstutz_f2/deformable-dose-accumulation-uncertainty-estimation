%% DDA_Uncertainty_main

%% Author: F.Amstutz, 20.06.2022
% florian.amstutz@psi.ch
% Code from: Florian Amstutz et al 2021 Phys. Med. Biol. 66 105007
% Please cite if you use this code


%% Input Parameters General
% Define the input parameters (what should be calculated)

% Define input parameter struct
input_parameter ={};

% Choose paths to data
input_parameter.data_base_path = '/yourpath/to/the/folder/';
input_parameter.structures_base_path = strcat(input_parameter.data_base_path,'Data/');
input_parameter.results_base_path = strcat(input_parameter.data_base_path,'Results/');

% Choose Patients (Format {'1','2','3' etc.})
input_parameter.Patients = {'1'};

% Choose repeated CTs to investigate (DIRs applied to reference) (Format {'1','2','3', etc.})
input_parameter.CTnumbers = {'1'};


% Choose Structures (Format {'BODY','DIBH_Heart', etc.}). If {} all
% structures are included
input_parameter.Structures = {'BODY','PTV','Parotid_L','Parotid_R','OpticNerve_L','OpticNerve_R','Chiasm'};


% Choose Methods
% E.g.  Elastix, Plastimatch_Bspline, Plastimatch_Demon, Velocity,
input_parameter.methods = {'Elastix','Plastimatch_Bspline','Plastimatch_Demon','Velocity'};

% Define a short string standing for the methods (e.g. 'abcd' a=Elastix, b= Plastimatch_Bspline
% c = Plastimatch_Demon, d = Velocity. If a new method or other combination
% are tried, results are stored in the respective folder
input_parameter.method_letters = {'abcd'};

% Recalculate existing data (If 'On' everything calculated again if 'Off'
% only the non-existing data calculated to save time)
input_parameter.recalculation_flag = 'On';

% Select calculation parts ('On' or 'Off')
input_parameter.calculation_part1_flag = 'On';
input_parameter.calculation_part2_flag = 'On';
input_parameter.calculation_part3_flag = 'On';


%% Input Parameters 1/3 - Mean DVF Uncertainty Maps
% Define the input parameters (what should be calculated)

% Calculate Max-Min spherical coordinates uncertainty measure
input_parameter.Execute_MinMax = 'On';
% Save values from DVFs and uncertainty maps to txt files for correlation
% calculation in part 2
input_parameter.Execute_CorrelationData = 'On';


%% Input Parameters 2/3 - Modelled Uncertainty Maps

% Which method should be the predictor of the uncertainty in the model
input_parameter.predictor_type = 'Plastimatch_Bspline';
% Which uncertainty should be predicted ('Max-Min') as long as no other
% uncertainty map proposed
input_parameter.predictor_uncertainty = 'Max-Min';
% With which CT should the correlation factor be determined (e.g. '1', 
% usually for first fraction model and then applied to later fractions)
input_parameter.prediction_CTnumbers = {'1'};
% For which repeated CTs an uncertainty map should be modelled (e.g. '2','3',..)
input_parameter.create_CTnumbers = {'2','3'};

%% Input Parameters 3/3 - Dose Uncertainty Method

% Define which DIR you used for correlation
selected_DIR = 'Plastimatch_Bspline';

% Uncertainty Method, 'Max-Min','Created
% _Uncertainty_Map_Plastimatch_Bspline; if 'Max-Min' chosen the
% 'ground-truth' geometric uncertainty map is used (GU-path); if 'Created_Uncertainty
% _Map_x the modelled geometric unc. map is used (GMU-path)
input_parameter.uncertainty_method = strcat('Created_Uncertainty_Map_',selected_DIR);
% input_parameter.uncertainty_method = 'Max-Min'; % This would lead to the GU-path in the paper

% Direction of which DIR for calculating direction factor (angle to dose
% gradient)
input_parameter.uncertainty_direction_factor = selected_DIR;



%% Calculations 1/3 - Uncertainty Map Calculations/Correlation data/Building Model
% Calculation of the mean deformation vector fields and the uncertainty
% maps

if strcmp(input_parameter.calculation_part1_flag, 'On')
    disp('Calculation Part 1 - Calculate geo uncertainty maps and get correlation data')
    f_DDA_Uncertainty_geo_Uncertainty(input_parameter);
end

%% Calculations 2/3 - Modelled Uncertainty Maps Calculations
% Calculation of the modelled uncertainty maps

if strcmp(input_parameter.calculation_part2_flag, 'On')
    disp('Calculation Part 2 - Calculate modelled uncertainty maps')
    f_modelled_geo_Uncertainty(input_parameter);
end

%% Calculations 3/3 - Dose Uncertainty Method
% Calculation of the dose uncertainty map and evaluation

if strcmp(input_parameter.calculation_part3_flag, 'On')
    disp('Calculation Part 3 - Calculation and evaluation of dose uncertainty maps')
    f_dose_Uncertainty(input_parameter);
   
end
