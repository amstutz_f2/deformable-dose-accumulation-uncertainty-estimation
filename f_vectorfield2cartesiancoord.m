function [outputvectorfield] = f_vectorfield2cartesiancoord(inputvectorfield)
%f_svectorfield2cartesiancoord Converts a vectorfield in cartesian
%coordinates to one spherical coordinates
%   Takes a vectorfield in cartesian coordinates and transforms the
%   vectorfield to spherical coordinates

phi_component = inputvectorfield(:,:,:,1);
theta_component = inputvectorfield(:,:,:,2);
r_component = inputvectorfield(:,:,:,3);

[x,y,z] = sph2cart(phi_component,theta_component,r_component);

outputvectorfield = cat(4,x,y,z);
end