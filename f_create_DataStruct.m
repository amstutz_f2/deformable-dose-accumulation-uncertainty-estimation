function [data_struct] = f_create_DataStruct(patient_string,CT_string,methods)
%f_create_DataStruct Creates empty data struct according to patients,
%CTnumbers, methods
%   Initializing an empty data struct according to
%   patients,CTnumbers,methods. 
%   Code from: Florian Amstutz et al 2021 Phys. Med. Biol. 66 105007
%   Please cite if you use this code, for research purposes only
%   florian.amstutz@psi.ch

    % Initialize struct
  	data_struct.(patient_string)=[];
  	for j=1:length(methods)
         	method_name = string(methods(j));
           	data_struct.(patient_string).(method_name) = [];

            xyz_indices_mask = 'xyz_indices_mask';
            index_indices_mask = 'index_indices_mask';
            data_struct.(patient_string).(xyz_indices_mask).(CT_string)=[];
            data_struct.(patient_string).(index_indices_mask).(CT_string)=[];
            data_struct.(patient_string).(method_name).(CT_string)=[];
  	end
    
end % end function

