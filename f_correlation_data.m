function [data_struct, Info] = f_correlation_data(...
    data_base_path, structures_base_path, results_base_path, data_struct, patient_number,patient_string, ...
    methods, method_letters, CT_number,CT_string)
%f_correlation_data Stores the values of the DVFs into a txt file to build linear model in step 2.
%   Code from: Florian Amstutz et al 2021 Phys. Med. Biol. 66 105007
%   Please cite if you use this code, for research purposes only
%   florian.amstutz@psi.ch


%% Calculations 
    
    Body_index_indices= data_struct.(patient_string).index_indices_mask.CT0.BODY;
    
    %
    for j=1:length(methods)
        method_string = string(methods{j});

        vectorfield_datafile = strcat(data_base_path,'Data/',patient_string,'/DIRs/',...
           method_string,'/',method_string,'_0','_',CT_number,'.mha');
        [data_struct.(patient_string).(method_string).(CT_string),Info] = readmha(vectorfield_datafile);

        % R
        tmp = (f_vectorfield2sphericalcoord(data_struct.(patient_string).(method_string).(CT_string)));
        tmp = tmp(:,:,:,3);
        Method_SpherCoord_flat = reshape(tmp,[],1);
        Method_SpherCoord_flat(setdiff(1:end,Body_index_indices)) = NaN;
        folderpath = strcat(results_base_path,'/',patient_string,'/Correlation_Data/Methods_',method_letters,'/');
        f_create_folder(folderpath);
        Method_SpherCoord_flat = Method_SpherCoord_flat(~isnan(Method_SpherCoord_flat));
        writematrix(Method_SpherCoord_flat,strcat(folderpath,method_string,'_r_CT',CT_number,'.txt'));
         
        
        clear tmp Method_SpherCoord_flat
    end % end loop over methods          
end % end function

