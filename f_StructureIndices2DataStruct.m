function [data_struct] = f_StructureIndices2DataStruct(structures_base_path, ...
    data_struct,patient_string,CT_number)
%F_IndicesStructures2DataStruct:HN Stores indices of structures into data
%struct.
%   Stores indices of structures into data
%   struct. Important later to know which voxels belong to which structure
%   Code from: Florian Amstutz et al 2021 Phys. Med. Biol. 66 105007
%   Please cite if you use this code, for research purposes only
%   florian.amstutz@psi.ch
    
    %Structure path
   	pathStructures = strcat(structures_base_path,patient_string,'/CTs/MHAs/',CT_number,'/structures/');
    %Iterate over files and get also structure name out
    files = dir(strcat(pathStructures,'*.mha'));

    %Iterate over structure files
    for file = files'
        [StructureMask,~] = readmha(strcat(pathStructures,file.name)); % Read the binary structure
        ind = find(StructureMask); % Find the indices of the structure (1 values in mha)
        [x_nonzeros,y_nonzeros,z_nonzeros] = ind2sub(size(StructureMask),ind); % Change the indices to the 3D representation

        % Define the structure name for saving into the struct
        structure_name = erase(file.name,'.mha');
        if ~isvarname(structure_name)
            structure_name = structure_name(~isspace(structure_name));
        end
        
        % Save into the struct
        xyz_indices_mask = 'xyz_indices_mask';
        index_indices_mask = 'index_indices_mask';
        CT_string = strcat('CT',CT_number);
        data_struct.(patient_string).(xyz_indices_mask).(CT_string).(structure_name) = [x_nonzeros,y_nonzeros,z_nonzeros];
        data_struct.(patient_string).(index_indices_mask).(CT_string).(structure_name) = ind;               
    end
    
    %% Reference CT
   	pathStructures = strcat(structures_base_path,patient_string,'/CTs/MHAs/0/structures/');
    %Iterate over files and get also structure name out
    files = dir(strcat(pathStructures,'*.mha'));

    for file = files'
        [StructureMask,~] = readmha(strcat(pathStructures,file.name));
        ind = find(StructureMask);
        [x_nonzeros,y_nonzeros,z_nonzeros] = ind2sub(size(StructureMask),ind);

        structure_name = erase(file.name,'.mha');
        if ~isvarname(structure_name)
            structure_name = structure_name(~isspace(structure_name));
        end
        
        xyz_indices_mask = 'xyz_indices_mask';
        index_indices_mask = 'index_indices_mask';
        CTref = "CT0";
        data_struct.(patient_string).(xyz_indices_mask).(CTref).(structure_name) = [x_nonzeros,y_nonzeros,z_nonzeros];
        data_struct.(patient_string).(index_indices_mask).(CTref).(structure_name) = ind;               
    end
         
            

end

