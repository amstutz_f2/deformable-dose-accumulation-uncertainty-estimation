function f_create_folder(folder)
%create_folder Checks if a given folder exist, if not folder is created.
%   Input: Path to folder

if ~exist(folder, 'dir')
       mkdir(folder)
end

end

