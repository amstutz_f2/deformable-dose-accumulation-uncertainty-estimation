function [all_data_produced] = check_data(input_parameter,...
    patient_string,CT_string,CT_num,structure_name,method_letters)
%check_data Checks if all data is already calculated
%   Function for DDA Uncertainty workflow to check if certain data is
%   already calculated
%   Code from: Florian Amstutz et al 2021 Phys. Med. Biol. 66 105007
%   Please cite if you use this code, for research purposes only

all_data_produced = 1;

writefile = strcat(input_parameter.results_base_path,'/',patient_string,'/Dose_Uncertainty_Method/Direction_Factor/Methods_',method_letters,'/');
writefile = strcat(writefile,'Direction_Factor_',input_parameter.uncertainty_method,'_',...
    input_parameter.uncertainty_direction_factor,'_',structure_name,'_CT',CT_num,'.mha');
if ~isfile(writefile)
    all_data_produced = 0;
    return
end


writefile = strcat(input_parameter.results_base_path,'/',patient_string,'/Dose_Uncertainty_Method/Dose_Gradients/');
writefile = strcat(writefile,'Gradients_mag_sobel_',structure_name,'_CT',CT_num,'.mha');
if ~isfile(writefile)
    all_data_produced = 0;
    return
end

writefile = strcat(input_parameter.results_base_path,'/',patient_string,'/Dose_Uncertainty_Method/Dose_Gradients/');
writefile = strcat(writefile,'Gradients_phi_sobel_',structure_name,'_CT',CT_num,'.mha');
if ~isfile(writefile)
    all_data_produced = 0;
    return
end

writefile = strcat(input_parameter.results_base_path,'/',patient_string,'/Dose_Uncertainty_Method/Dose_Gradients/');
writefile = strcat(writefile,'Gradients_theta_sobel_',structure_name,'_CT',CT_num,'.mha');
if ~isfile(writefile)
    all_data_produced = 0;
    return
end


writefile = strcat(input_parameter.results_base_path,'/',patient_string,'/Dose_Uncertainty_Method/Dose_Gradients/');
writefile = strcat(writefile,'Gradients_mag_sobel_x',structure_name,'_CT',CT_num,'.mha');
if ~isfile(writefile)
    all_data_produced = 0;
    return
end


writefile = strcat(input_parameter.results_base_path,'/',patient_string,'/Dose_Uncertainty_Method/Dose_Gradients/');
writefile = strcat(writefile,'Gradients_mag_sobel_y',structure_name,'_CT',CT_num,'.mha');
if ~isfile(writefile)
    all_data_produced = 0;
    return
end


writefile = strcat(input_parameter.results_base_path,'/',patient_string,'/Dose_Uncertainty_Method/Dose_Gradients/');
writefile = strcat(writefile,'Gradients_mag_sobel_z',structure_name,'_CT',CT_num,'.mha');
if ~isfile(writefile)
    all_data_produced = 0;
    return
end


writefile = strcat(input_parameter.results_base_path,'/',patient_string,'/Dose_Uncertainty_Method/Dose_Gradients/');
writefile = strcat(writefile,'Gradients_mag_sobel_upscaled_',structure_name,'_CT',CT_num,'.mha');
if ~isfile(writefile)
    all_data_produced = 0;
    return
end
                

writefolder = strcat(input_parameter.results_base_path,'/',patient_string,...
 	'/Dose_Uncertainty_Method/Dose_Uncertainty/Methods_',method_letters,'/');               
writefile = strcat(writefolder,'Gradient_mag_sobel_times_Uncertainty_',input_parameter.uncertainty_method,'_',structure_name,'_CT_',CT_num,'.mha');
if ~isfile(writefile)
    all_data_produced = 0;
    return
end
                
writefile = strcat(writefolder,'Gradient_mag_sobel_times_Uncertainty_Angle_',input_parameter.uncertainty_direction_factor,'_',input_parameter.uncertainty_method,'_',structure_name,'_CT_',CT_num,'.mha');
if ~isfile(writefile)
    all_data_produced = 0;
    return
end
                
writefile = strcat(input_parameter.results_base_path,'/',patient_string,'/Dose_Uncertainty_Method/Difference_to_GT/Methods_',method_letters,'/');
writefile = strcat(writefile,'Gradients_Minus_DoseDiff_',structure_name,'CT',CT_num,'.mha');
if ~isfile(writefile)
    all_data_produced = 0;
    return
end

writefile = strcat(input_parameter.results_base_path,'/',patient_string,'/Dose_Uncertainty_Method/Difference_to_GT/Methods_',method_letters,'/');
writefile = strcat(writefile,'GradientsUncertainty',input_parameter.uncertainty_method,'_','Minus_DoseDiff_',structure_name,'_CT',CT_num,'.mha');
if ~isfile(writefile)
    all_data_produced = 0;
    return
end

writefile = strcat(input_parameter.results_base_path,'/',patient_string,'/Dose_Uncertainty_Method/Difference_to_GT/Methods_',method_letters,'/');
writefile = strcat(writefile,'GradientsUncertaintyAngle_',input_parameter.uncertainty_direction_factor,'_',input_parameter.uncertainty_method,'_','Minus_DoseDiff_',structure_name,'_CT',CT_num,'.mha');
if ~isfile(writefile)
    all_data_produced = 0;
    return
end


structure_name = strrep(structure_name,' ','_');
struct_path = strcat(input_parameter.results_base_path,patient_string,'/Dose_Uncertainty_Method/Histogram_Data/');

if ~isfile(strcat(struct_path,patient_string,'_',CT_string,'_',structure_name,'_',...
    method_letters,'_',input_parameter.uncertainty_method,'_','G_DD_Histogram_Data.mat'))  
    all_data_produced = 0;
    return
end

if ~isfile(strcat(struct_path,patient_string,'_',CT_string,'_',structure_name,'_',...
    method_letters,'_',input_parameter.uncertainty_method,'_','GU_DD_Histogram_Data.mat')) 
    all_data_produced = 0;
    return
end
if ~isfile(strcat(struct_path,patient_string,'_',CT_string,'_',structure_name,'_',...
                    method_letters,'_',input_parameter.uncertainty_method,'_',input_parameter.uncertainty_direction_factor,'_','GUA_DD_Histogram_Data.mat')) 
    all_data_produced = 0;
    return
end


end

