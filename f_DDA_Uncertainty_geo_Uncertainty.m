function [] = f_DDA_Uncertainty_geo_Uncertainty(input_parameter)
%f_DDA_Uncertainty_geo_Uncertainty Calculated the mean DVFs of a set of
%different DVFs and the Max-Min uncertainty map.
%   A datastruct is constructed to work with the data. The indices of
%   different structures are added to the data struct. The mean DVF of a
%   group of DVFs is calculated and saved. The max-min uncertainty map of
%   the group of DVFs is calculated and saved. In a last step the values of
%   the single DVFs and the values of the uncertainty map are stored in txt
%   files to generate the linear model in step 2.
%   Code from: Florian Amstutz et al 2021 Phys. Med. Biol. 66 105007
%   Please cite if you use this code, for research purposes only
%   florian.amstutz@psi.ch

    % Iterate over the investigated patients
    for i=1:length(input_parameter.Patients)
        patient_number = string(input_parameter.Patients{i});
        patient_string = strcat('Patient_',patient_number);
        % Iterate over the investigated CTs
        for k=1:length(input_parameter.CTnumbers)
            CT_number = string(input_parameter.CTnumbers{k});
            CT_string = strcat('CT',CT_number);
            disp(strcat('...........',patient_string,',',CT_string,'.............'))

            % Initialize data struct
            disp('Create data struct...')
            data_struct = f_create_DataStruct(patient_string,CT_string,input_parameter.methods);
            disp('Done.')

            % Store indices of structures into data struct
            disp('Save structure indices to data struct...')
            data_struct = f_StructureIndices2DataStruct(input_parameter.structures_base_path,...
                data_struct,patient_string,CT_number);
            disp('Done.')


            % Calculation max matrix, min matrix and max-min matrix of r in spherical coordinates 
            % of included methods
            if strcmp(input_parameter.Execute_MinMax,'On')
                disp('Calculate max, min, max-min matrix...')
                tic
                f_MaxMin_DVF(input_parameter.structures_base_path,input_parameter.results_base_path,data_struct,patient_string,input_parameter.methods,...
                    input_parameter.method_letters, CT_number,CT_string);
                toc
                disp('Done.')
            end

            % Get the DVFs values and store them to
            % individual txt files to create the linear model in step 2.
            if strcmp(input_parameter.Execute_CorrelationData,'On')
                disp('Calculate correlation data...')
                tic
                f_correlation_data(input_parameter.data_base_path,...
                    input_parameter.structures_base_path, input_parameter.results_base_path,data_struct,...
                    patient_number,patient_string,input_parameter.methods,...
                    input_parameter.method_letters,CT_number,CT_string);
                toc
                disp('Finished!')
            end

 
            clearvars -except input_parameter patient_string patient_number 
        end % end CTs loop
    end % end patient loop

end % end fuction DIR_Uncertainty_f_Calculation_1

